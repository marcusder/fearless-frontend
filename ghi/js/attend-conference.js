window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }


        const spinnertag = document.getElementById('loading-conference-spinner');
        selectTag.classList.remove("d-none");
        spinnertag.classList.add("d-none");
    }

    const formTag = document.getElementById('create-attendee-form')
    formTag.addEventListener('submit', async event =>{
    event.preventDefault();
    const formData = new FormData(formTag)
    const jsonFormData = JSON.stringify(Object.fromEntries(formData));
    console.log(jsonFormData)
    const attendeeURL = 'http://localhost:8001/api/attendees/'
    const fetchConfig ={
        method:"post",
        body: jsonFormData,
        headers:{
            'Content-Type': 'application/json',
      }
    };
    const response = await fetch (attendeeURL,fetchConfig)
    if(response.ok){
        let successTag =document.getElementById("success-message")
        successTag.classList.remove("d-none")
        formTag.classList.add("d-none")
      formTag.reset();
      const newConference =await response.json()
      console.log(newConference)

}
});
  });
