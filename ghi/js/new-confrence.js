window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      const selectTag =document.getElementById("location");
      for (let location of data.locations){
        const option = document.createElement("option");
        option.value =location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag)
    const jsonFormData = JSON.stringify(Object.fromEntries(formData));
    console.log(jsonFormData)
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: jsonFormData,
        headers: {
        'Content-Type': 'application/json',
  },
};
const response = await fetch(conferenceUrl, fetchConfig);
if (response.ok) {
formTag.reset();
const newLocation = await response.json();
console.log(newLocation);

}
});
  });
