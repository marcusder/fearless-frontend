function createCard(name, description, pictureUrl, starts, ends, locationname) {
    return `
      <div class="card">
      <div class="shadow p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationname}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-transparent border-success">${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}</div>
        </div>
      </div>

    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if(!response.ok) {
            console.error('An error has occured');
        } else {
            const data = await response.json();
            const columns = document.querySelectorAll('.col');
            let columnIndex = 0;
            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const locationname = details.conference.location.name
                const html = createCard(title, description, pictureUrl,starts,ends,locationname);
                // const columns = document.querySelector('.col');
                // let columnIndex = 0;
                const column = columns[columnIndex];
                column.innerHTML += html;
                columnIndex = (columnIndex + 1) % columns.length

                console.log(details)
              }
            }
        }
            } catch (e) {
                throw new Error("Network response was not okay")
            }
          });
